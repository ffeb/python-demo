FROM python:3.10.2-alpine3.15

ENV PYTHONUNBUFFERED=1

WORKDIR /app

COPY requirements.txt .

RUN python3 -m pip install -r requirements.txt --no-cache-dir

RUN adduser -D user

USER user

COPY src/ .

CMD [ "python3", "-m" , "flask", "run", "--host=0.0.0.0"]
